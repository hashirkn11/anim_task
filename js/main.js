let section_2 = gsap.timeline({
  scrollTrigger: {
    trigger: ".animate_sec",
    start: "top top",
    end: "+=1200",
    pin: true,
    scrub: 0.5,
  },
});
// section_2.to(".animate_sec .anim_img", {
//   // big the image
//   duration: 1,
//   scale: 1.5,
// });
section_2.fromTo(
  ".animate_sec .anim_img",
  {
    duration: 1,
    scale: 1.5,
  },
  {
    scale: 1,
  }
);

// from opacity 0 to 1
section_2.fromTo(
  ".animate_sec .mobile_menu",
  {
    duration: 1,
    opacity: 0,
  },
  {
    opacity: 1,
  }
);

// section_2.fromTo(
